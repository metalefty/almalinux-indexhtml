Name: almalinux-indexhtml
Version: 8
Release: 7.1%{?dist}
Summary: Browser default start page for AlmaLinux
Source: %{name}-%{version}-7.1.tar.gz
License: Distributable
Group: Documentation
BuildArch: noarch

Obsoletes: redhat-indexhtml
Obsoletes: centos-indexhtml

Provides: redhat-indexhtml = %{version}-%{release}
Provides: centos-indexhtml = %{version}-%{release}

%description
The indexhtml package contains the welcome page shown by your Web browser,
which you'll see after you've successfully installed AlmaLinux.

The Web page provided by indexhtml tells you how to register your AlmaLinux
software and how to get any support that you might need.

%prep
%setup -q -n HTML

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML
cp -a . $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML/


%files
%{_defaultdocdir}/HTML/*

%changelog
* Tue Jun 08 2021 Andrei Lukoshko <alukoshko@almalinux.org> - 8-7.1
- Update tarball

* Wed Jan 20 2021 Anatholy Scryabin <ascryabin@cloudlinux.com> - 8-7
- Initial build for AlmaLinux
